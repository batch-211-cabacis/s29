

// Find Users with letter 's' in ther first name or 'd' in their last name
// Show only first name and last name and hide id field


db.users.find(
	{$or: 
		[{firstName: {$regex: 's', $options: '$i'}}, 
		{lastName: {$regex: 'd', $options: '$i'}}
		]},
		{
		firstName: 1,
		lastName: 1,
		_id: 0
		}
	);


// Find users from HR department, age is gte to 70
// use $and operator

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});



// Find users with letter e in firstname and has age of lte to 30
// use $regex $and and $lte

db.users.find({$and: [{firstName: {$regex: 'e', $options: '$i'}}, {age: {$lte: 30}}]});