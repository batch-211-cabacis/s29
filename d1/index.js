/*
	// MongoDB - Query Operators
	// Expand Queries in MongoDB using Query Operators
*/ 
/*
	Overview
	A. Query operator
	- definition
	- importance

	Type of query operators
	1. Comparison query operators
		1.1 greter than
		1.2 greater than or equal to
		1.3 less than
		1.4 less than or equal to
		1.5 not equal to
		1.6 In
	2. Evaluation query operator
		2.1 Regex
			2.1.1 case sensitive query
			2.1.2 case insensitive query

	3. Logical query operators
		3.1 OR
		3.2 AND

	B. Field projection
	1. Inclusion
		1.1 returning specific fields in embedded documents
		1.2 exceptionto the inclusion rule
		1.3 slice operator

	2. Exclusion
		2.1 excluding specific fields in embedded documents
*/

/*
	Query operators
		- a "query" is a request for data from database
		- an "operator" is a symbol that represents an action or a process
		- outting them together, they mean the things that we can do on our queries using certain operators 
*/

/*
	Why do we need to study query operators?
	- knowing query operators will enable us to create queries that can do more than what a simple operators do. (we can do more than what we do in simple CRUD operations)
	- example: in CRUD operations discussion we have discussed findOne using a specific value inside its single or multiple parameters when we know query operators, we may look for a record that are more specific
*/

// 1. COMPARISON Query Operators

/*
	Includes: 
		-greater than
		-less than
		-greater than or equal to
		-less than or equal to
		-Not equal to
		-In (finds  multiple values)
*/

/*
	1.1 Greater than "$gt"
	- "$gt" finds documents that have field numbers that are greater  than specified value
	SYNTAX
		d.collectionName.find({field: {$gt}})
*/
db.users.find({age: {$gt: 76}});

/*
	1.2 Greater than or Equal to
	- "$gte" finds documents that have field number values that are greater than or equal to specific values
	SYNTAX
		db.collectionName.find({field: {$gte: value}})
*/
db.users.find({age: {$gte: 76}});

/*
	1.3 Less Than oeprator
	- "$lt" finds documents that have field number values less than the specified value
	SYNTAX
		db.collectionName.find({field: {$lt: value}})
*/
db.users.find({age: {$lt: 65}});

/*
	1.4 Less Than or equal to operator
	- "$lte" finds documents that have field number values less than or equal to the specified value
	SYNTAX
		db.collectionName.find({field: {$lte: value}})
*/
db.users.find({age: {$lte: 65}});

/*
	1.5 Not equal to operator
	- "$ne" finds documents that have field number that are not equal to the specified value
	SYNTAX
		db.collectionName.find({field: {$ne: value}})
*/
db.users.find({age: {$ne: 65}});

/*
	1.6 In operator
	- "$in" finds documents with specific match criteria on one field using different values
	SYNTAX
		db.collectionName.find({field {$in: [value1, value2, value3]}})
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});



// 2. EVALUATION query operators
// - evaluation operators return data based on evaluations of either individual fields or the entire collection's documents

/*
	2.1 Regex
	- "$regex" is short for regular expression
	- they are called regular expressions because they are based on regular languages
	- regex is used for matching strings
	- it allows us to find documents that match a specific string pattern using regular expressions
*/
/*
	2.1.1 Case sensitive query
	SYNTAX
		db.collectionName.find({field: {$regex: 'pattern'}})
*/
db.users.find({lastName: {$regex: 'A'}});

/*
	2.1.2 Case Insensitive query
	- "$i" we can run case insensitive queries by utilizing the "i" option
	SYNTAX
		db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}})
*/
db.users.find({lastName: {$regex: 'A', $options: '$i'}});


// Mini Activity 1
db.users.find({firstName: {$regex: 'E', $options: '$e'}});

// 3. Logical Operators
/*
	3.1 OR Operator
	- "$or" finds documents that m,atch a single criteria from multiple provided search criteria
	SYNTAX
		db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})
*/

db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

/*
	3.2 AND Operator
	- "$and finds documents matching multiple criteria in a single field
	SYNTAX
		db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]})
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

// Mini activity 2
db.users.find({$and: [{firstName: {$regex: 'e'}}, {age: {$lte: 30}}]});


// FIELD PROJECTION
/*
	- by default mongoDB returns the whole document especially when dealing with complex documents, but sometimes it is not helpful to view the whole document especially when dealling with complex documents
	- to help with readability of the values returned (or sometimes because of security reasons), we include or exclude some fields.
	- in otehr words we project our selected fields
*/
/*
	1. Inclusion
	- allows us to include or add specific fields only when retrieving documents
	- the value denoted is "1" to indicate that the field ois being incuded
	- we cannot do exclusion on field that uses inclusion projection
	SYNTAX
		db.collectionName.find({criteria}, {field:1})
*/\
db.users.find({firstName: "Jane"});

db.users.find(
	{
	firstName: "Jane"
	},
	{	firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

/*
	1.1 RETURNING SPECIFIC FIELDS IN EMBEDDED DOCUMENTS
	- the double quotation are important

*/

db.users.find(
	{
	firstName: "Jane"
	},
	{	firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

/*
	1.2 EXCEPTION TO THE INCLUSION RULE: Suppressing the ID field
	- allows us to exclude the "_id" field when retrieveing documents
	- when using field projection, field inclusion and eclusion may not be used at the same time
	- excluding the "_id" field is the ONLY exception to this rule
	SYNTAX
		db.collectionName.find({criteria}, {_id:0})
*/
db.users.find(
	{	firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);


/*
	1.3 SLICE OPERATOR
	- "$slice" operator allows us to retrieve only one element that matches the search criteria
*/
// to demonstrate....

// db.users.insertOne({
// 	namearr: [
// 	{
// 		nameA: "Juan",
// 	},
// 	{
// 			nameB: "Tamad"
// 	}

// 	]
// });
// db.users.find({namearr: {nameA: "Juan"}});

// // using a slice operator
// db.users.find(
// {
// 	"namearr":
// 	{
// 		nameA: "Juan"
// 	} 
// 	{
// 		namearr:
// 			{$slice: 1}
// 	}
// });

// Mini Activity 3

	db.users.find(
		{firstName: {$regex: 's', $options: '$i'}},
		{
		firstName: 1,
		lastName: 1,
		_id: 0
		}
);

/*
	2. EXCLUSION
	- allows us to exclude or remove specific fields when retrieving documents
	- the value provided is zero to denote that the field is being excluded
	SYNTAX
		db.collectionName.find({criteria}, {field: 0})
*/
db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}
);

/*
	2.1 EXCLUDING/SUPPRESSING SPECIFIC FIELDS IN EMBEDDED DOCUMENTS
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0	
	}
);
